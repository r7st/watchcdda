#!/usr/bin/env ksh

NVAL=5
NPROC=1
NDIR=Cataclysm-DDA-$(date "+%Y_%m_%d")

buildGame() { 
  git pull
  nice -n$NVAL make -j$NPROC RELEASE=1 USE_HOME_DIR=1 LTO=1
  cd .. && cp -r Cataclysm-DDA $NDIR
}

prepGame() {
  cd $NDIR
  rm -fr .git
  cp cataclysm cataclysm-group
  mv cataclysm cataclysm-exp
  cp cataclysm-launcher cataclysm-launcher-group
  sed -i '/^BIN=/s/=.*/=cataclysm-exp/' cataclysm-launcher
  sed -i '/^BIN=/s/=.*/=cataclysm-group/' cataclysm-launcher-group
  make clean
}

unObs() {
  Files=($(find data/mods -type f -name modinfo.json))
  for File in "${Files[@]}"; do
    test -z $(sed -n '/obsolete.*true/p' < "$File") || obs+=("$File")
  done
  for o in "${obs[@]}"; do
    sed -ri -e '/name/s/(.*: ")(.*)(".*)/\1\2 (obsolete)\3/'\
      -e '/obsolete/s/true/false/' "$o"
  done
}

buildGame
prepGame
unObs
cd ..
tar zcf $NDIR.tgz $NDIR

