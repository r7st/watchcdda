#!/bin/bash

NUMLINES=100
entCmd="journalctl -e -u systemd-logind --no-pager | tail -$NUMLINES"
while read -r ent; do
  in=$(sed -nr '/New session/s/(.*) '"$HOSTNAME"'.*session ([0-9]+) of user (.+)\.$/\2 \3 \1/p' <<< "$ent")
  out=$(sed -nr '/logged out/s/(.*) '"$HOSTNAME"'.*Session ([0-9]+) logged out.*/\2 \1/p' <<< "$ent")
  sessions+=("$in")
  sessions+=("$out")
done < <(eval $entCmd)

IFS=$'\n' sSessions=($(sort -n -k1 <<< "${sessions[*]}")); unset IFS

for ((i = 1; i < "${#sSessions[@]}"; i++)); do
  inSession="${sSessions[i]}"
  outSession="${sSessions[i-1]}"
  cInd=$(cut -d" " -f1 <<< "$outSession")
  pInd=$(cut -d" " -f1 <<< "$inSession")
  test $cInd -eq $pInd || continue
  if [ "${#inSession}" -lt "${#outSession}" ]; then
    t=$inSession
    inSession=$outSession
    outSession=$t
  fi
  Player=$(cut -d" " -f2 <<< "$inSession")
  inTime=$(sed -r 's/^[0-9]+ [^ ]+ //' <<< "$inSession")
  outTime=$(sed -r 's/^[0-9]+ //' <<< "$outSession")
  inTimeConv=$(date -d "$inTime" -Is)
  outTimeConv=$(date -d "$outTime" -Is)
  timeDiff=$(datediff "$inTimeConv" "$outTimeConv" -f "%Hh%M")
  test $timeDiff != "0h0" || continue
  printf "PLAYER: %-15s IN [%s] | OUT [%s] (%s).\n" "$Player" "$inTime" "$outTime" "$timeDiff"
done

