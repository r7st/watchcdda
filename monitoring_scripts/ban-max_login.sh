#!/bin/bash

BANTIME="1h"
MAX_LOGINS=3

allIPs=($(who | sed -r 's/.*\(([0-9\.]+)\)/\1/'))
uniqIPs=($(echo "${allIPs[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
for IP in "${uniqIPs[@]}"; do
  withoutIP=(${allIPs[@]/$IP})
  numIP=$(expr "${#allIPs[@]}" - "${#withoutIP[@]}")
  echo "$IP occurs $numIP times"
  if [ "$numIP" -gt "$MAX_LOGINS" ]; then
    echo "$IP is logged in too many times!"
    firewall-cmd --timeout=$BANTIME --add-rich-rule="rule family='ipv4' source address=$IP reject"
  fi
done

