#!/bin/bash

BANTIME="1h"
MAX_LOGINS=3

players=(); ips=()
entries=($(who | sed -re 's#^([^ ]+).+\((.+)\)#\1,\2#' -e 's/:.*//g'))
for entry in "${entries[@]}"; do
  players+=($(cut -d"," -f1 <<< "$entry"))
  ips+=($(cut -d"," -f2 <<< "$entry"))
done

ips=($(tr ' ' '\n' <<< "${ips[@]}" | sort -u | tr ' ' '\n'))
entries=($(tr ' ' '\n' <<< "${entries[@]}" | sort -u | tr ' ' '\n'))

banIps=()
for ip in "${ips[@]}"; do
  numAcc=0
  for entry in "${entries[@]}"; do
    if grep "$ip" <<< "$entry" > /dev/null 2>&1; then
      numAcc=$(( numAcc + 1 ))
    fi
  done
  if [ "$numAcc" -ge "$MAX_LOGINS" ]; then
    firewall-cmd --timeout=$BANTIME --add-rich-rule="rule family='ipv4' source address=$ip reject"
    banIps+=("$ip")
  fi
done

kickUsers=()
for ip in "${banIps[@]}"; do 
  for entry in "${entries[@]}"; do
    kickUsers+=("$(sed -nr 's/([^,]+),'"$ip"'/\1/p' <<< "$entry")")
  done
done

for kick in "${kickUsers[@]}"; do
  if [ "$kick" != "root" ]; then
    killall -u "$kick"
  fi
done

