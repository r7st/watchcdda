#!/bin/bash

QUIT() {
  echo "[$1]"
  exit 1
}

echo "[$(date -Is)] taking backups"

if mountpoint -q /backups; then
  cd /backups
else
  QUIT "/backups is not mounted."
fi

DNOW=$(date -I | sed s/-/_/g)
BDIR=SAVES-$DNOW
mkdir $BDIR 2>/dev/null
test $? -eq 0 || QUIT "Could not create save dir"

/usr/bin/rsync -qaPe "ssh -p2222" root@localhost:/home/* $BDIR/ >/dev/null 2>&1
test $? -eq 0 || QUIT "Could not rsync backups"

nice -n10 tar -jcf $BDIR.tbz2 $BDIR >/dev/null 2>&1
test $? -eq 0 || QUIT "Could not compress backups"

rm -rf $BDIR
test $? -eq 0 || QUIT "Could not remove unarchived backups"
echo "[$(date -Is)] backups complete"
