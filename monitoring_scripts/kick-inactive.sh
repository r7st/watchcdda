#!/bin/bash

SCRIPTPATH=$(dirname $0)

noWatch="root|multiplayer"
matchGames="cataclysm-exp|cataclysm|cataclysm-group|nethack|angband|crawl|harmonist"
loggedInUsers=($(ps -ax -ouser:32=,cmd= | sed -rn -e '/^(sshd|root)/d' -e '/sshd:/s/^([^ ]+) .*/\1/p' | sort -u))
playingUsers=($(ps aouser:32=,cmd:100= | sed -nr '/^'"$noWatch"'/!s/^([^ ]+).*\/('"$matchGames"')$/\1/p'))


if [ "x${playingUsers[0]}" = "x" ]; then exit 0; fi
for playing in "${playingUsers[@]}"; do
  loggedIn=0
  for logged in "${loggedInUsers[@]}"; do
    if [ "$playing" = "$logged" ]; then loggedIn=1; fi
  done
  if [ "$loggedIn" -eq 0 ]; then
    #echo "$playing" is playing but not logged in!
    printf "[%s] KICKED: %s\n" $(date -Is) $playing >> $SCRIPTPATH/logs/kick-log.out
    killall -u $playing
  fi
done

