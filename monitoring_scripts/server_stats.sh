#!/bin/bash

SCRIPTDIR=$(dirname $0)

players=($(find /home -maxdepth 1 -type d)); myaccs=6
printf "NUM PLAYERS: %d\n\n" $(expr "${#players[@]}" - $myaccs)

rootPer=$(df -h | sed -rn '/\/$/s/.* ([0-9.]+)% .*/\1/p')
((rootPer > 80)) && printf "WARNING: "
printf "$rootPer%% disk used on ROOT\n\n"

freeMem=$(free -m | sed -rne 's/ +/ /g' -e '/^Mem/s/.* ([0-9]+)$/\1/p')
((freeMem < 500)) && printf "WARNING: "
printf "${freeMem}M FREE\n\n"

printf "PROCESS BY MEM:\n"
top -sbn1 -o%MEM | sed -n '/^[ 0-9]\+/p' | head -n11

printf "\nRECENT PLAYERS:\n"
$SCRIPTDIR/player_time.sh | egrep -v "r7st|root" | tail -5

noWatch="root|multiplayer"
matchGames="cataclysm-exp|cataclysm|cataclysm-group|nethack|angband|crawl|harmonist"
loggedInUsers=($(ps -ax -ouser:32=,cmd= | sed -rn -e '/^(sshd|root)/d' -e '/sshd:/s/^([^ ]+) .*/\1/p' | sort -u))
playingUsers=($(ps aouser:32=,cmd:100= | sed -nr '/^'"$noWatch"'/!s/^([^ ]+).*\/('"$matchGames"')$/\1/p'))

printf "\nUSERS LOGGED IN:\n"
test "${#loggedInUsers[@]}" -eq 0 && echo None
echo "${loggedInUsers[*]}"

printf "\nUSERS PLAYING:\n"
test "${#playingUsers[@]}" -eq 0 && echo None
echo "${playingUsers[*]}"

