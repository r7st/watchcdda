#!/bin/bash

us=($(ls /home | grep -v sshchat))
while [ "$inp" != "DONE" ]; do
  for u in "${us[@]}"; do ps -u$u -ouser:32=,pid=,comm=; done
  printf "\nKILL: "; read inp 
  test -z $inp && continue
  test $inp = DONE && exit 0
  killall -u "$inp" 2>/dev/null
done

